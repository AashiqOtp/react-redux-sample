import {createStore} from 'redux';

const initialState=[];

const reducer = (state=initialState,action)=>{
    if (action.type==="ADD") {
        return state=[...state,action.payload]      
    }
    if (action.type==="REMOVE") {
        return state=state.filter(todo=>{return todo.id !== action.payload.id}) 
    }
    if (action.type==="MARK_DONE") {
        return state=state.map((todo,i)=>{
              return state = todo.id===action.payload.id ?  {...todo,isDone:true} : {...todo}
        }) 
    }
    return state
}
export const Store = createStore(reducer);

