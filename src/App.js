import React from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import Todo from './components/todo';

function App() {
  return (
    <React.Fragment>
      <div className="h1 font-weight-bold text-dark text-center ">
        React-Redux-App
      </div>
      <Todo/>
    </React.Fragment>
  );
}

export default App;
