import React, { useState } from 'react';
import {useDispatch, useSelector} from 'react-redux';

const AddTodo = () => {
    const dispatch = useDispatch();
    const data = useSelector( state => state);
    const [todo, settodo] = useState('');
    return ( 
        <>
        <div className="row mt-5">
            <div className="offset-3 col-6">
                <input type="text" className="form-control text-secondary font-weight-bold" placeholder="Add Todo"
                onChange={(e)=>{settodo(e.target.value)}}
                />
            </div>
                <button className="btn btn-primary" onClick={()=>{
                    dispatch({type:'ADD',payload:{todo:todo,id:data.length+1,isDone:false}});
                }}>Add</button>

        </div>
           
        </>
     );
}
 
export default AddTodo;