import React from 'react';
import AddTodo from './addtodo';
import {useDispatch, useSelector} from 'react-redux'
const Todo = () => {

    const data = useSelector( state => state);
    const dispatch = useDispatch();
    console.log(data)
    return (
        <>
        <AddTodo/>
            {data.map((todo,id)=>{
                return <div className="row mt-5" key={id}>
                <div className="offset-2 col-6 text-dark font-weight-bold" key={todo.id}>
                   {todo.todo}
                </div>
                
                {!todo.isDone ? <>
                <button className="col-1 ml-1 btn btn-danger"
                onClick={()=>{
                    dispatch({type:'REMOVE',payload:{id:todo.id}})
                }}
                >
                    Delete
                </button>
                <button className="col-1 ml-1 btn btn-success"
                onClick={()=>{
                    dispatch({type:'MARK_DONE',payload:{id:todo.id}})
                }}
                >
                    Done
                </button>
                </>:<div className="text-success font-weight-bold col-2">Completed</div>}
            </div>}
            )}
        </>
      );
}
 
export default Todo;